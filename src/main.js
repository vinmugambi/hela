import { createApp } from "vue";
import App from "./App.vue";
import "./index.css";
import server from "./machines/server";

createApp(App).mount("#app");

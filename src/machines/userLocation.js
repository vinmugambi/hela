import { createMachine, assign, sendParent } from "xstate";
import { getCountryNames } from "../utils/supported";
import { extractCountryName, getUserCoordinates } from "../utils/location";

const sendersCountryMachine = createMachine({
  id: "senders-country",
  initial: "idle",
  context: {
    country: "",
    supported: [],
    default: "Kenya",
  },
  states: {
    idle: {
      entry: [assign({ supported: getCountryNames })],
      invoke: {
        id: "get-user-coordinates",
        src: getUserCoordinates,
        onDone: { target: "settingFromGeolocation" },
        onError: "settingDefault",
      },
    },
    settingFromGeolocation: {
      invoke: {
        id: "set-user-location-from-coordinates",
        src: async function (context, event) {
          try {
            let country = await extractCountryName(event.data);
            if (country && context.supported.includes(country)) {
              return country;
            } else
              return Promise.reject("Hela is not available in your country");
          } catch (err) {
            return Promise.reject(err.message);
          }
        },
        onDone: {
          target: "countrySet",
          actions: [assign({ country: (_, event) => event.data }), sendParent({type: "SET_COUNTRY"})],
        },
        onError: "settingDefault",
      },
    },
    settingDefault: {
      always: [
        {
          target: "countrySet",
          actions: assign({ country: (context) => context.default }),
        },
      ],
    },
    countrySet: {
      type: "final",
      data: {
        country: (context) => context.country,
      },
    },
  },
});

export default sendersCountryMachine;

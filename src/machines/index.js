import { createMachine, assign, forwardTo } from "xstate";
import { inspect } from "@xstate/inspect";
import userLocationMachine from "./userLocation";
import { getCountry } from "../utils/supported";
import PhoneNumber from "awesome-phonenumber";

inspect({
  iframe: false,
});

export const clientMachine = createMachine({
  id: "hela-client",
  initial: "loading",
  context: {
    countrySendingFrom: null,
    countrySendingTo: "",
    receiversNetwork: "",
    amountToSend: "",
    amountReceived: "",
    receiversMobile: "",
    sendersNetwork: "",
    sendersMobile: "",
    charges: 0,
    total: 0,
    errors: {},
  },
  states: {
    loading: {
      invoke: {
        id: "user-location",
        src: userLocationMachine,
        onDone: {
          target: "calculator",
          actions: assign({
            countrySendingFrom: (_, event) => {
              return event.data.country;
            },
          }),
        },
      },
    },
    calculator: {
      initial: "changing",
      onDone: {
        target: "receiversPhone",
      },
      states: {
        changing: {
          on: {
            CHANGE: {
              actions: assign(function (context, event) {
                return { ...context, [event.key]: event.value };
              }),
            },
            SEND: {
              target: "validating",
            },
          },
        },
        validating: {
          invoke: {
            id: "validate-calc",
            src: validateCalc,
            onError: {
              target: "invalid",
              actions: assign({
                errors: (_, event) => event.data,
              }),
            },
            onDone: {
              target: "valid",
            },
          },
        },
        invalid: {
          on: {
            CHANGE: {
              actions: assign(function (context, event) {
                return { ...context, [event.key]: event.value };
              }),
            },
            SEND: {
              target: "validating",
            },
          },
        },
        valid: {
          type: "final",
        },
      },
    },
    receiversPhone: {
      initial: "accepting",
      onDone: {
        target: "confirming",
      },
      states: {
        accepting: {
          on: {
            CHANGE_NUMBER: {
              target: "validatingPhone",
              actions: assign({
                receiversMobile: (_, event) => event.value,
              }),
            },
            CHANGE_RECEIVER_NET: {
              actions: assign({
                receiversNetwork: (_, event) => event.value,
              }),
            },
            SUBMIT: "submitting",
          },
        },
        validatingPhone: {
          invoke: {
            id: "verify-phone",
            src: (context) =>
              verifyPhone(
                context.receiversMobile,
                getCountry(context.countrySendingFrom).alpha
              ),
            onError: {
              target: "invalid",
              actions: assign({
                errors: (context, event) => {
                  let { errors } = context;
                  errors.receiversMobile = event.data;
                  return errors;
                },
              }),
            },
            onDone: {
              target: "accepting",
            },
          },
        },
        submitting: {
          invoke: {
            src: validateReceiversInfo,
            id: "validate-receiver-information",
            onDone: {
              target: "valid",
            },
            onError: {
              target: "invalid",
              actions: assign({
                errors: (context, event) => {
                  let { errors } = context;
                  Object.assign(errors, event.data);
                  return errors;
                },
              }),
            },
          },
        },
        invalid: {
          on: {
            CHANGE_NUMBER: {
              target: "validatingPhone",
              actions: assign({
                receiversMobile: (_, event) => event.value,
              }),
            },
            CHANGE_RECEIVER_NET: {
              actions: assign({
                receiversNetwork: (_, event) => event.value,
              }),
              target: "submitting",
            },
          },
        },
        valid: {
          type: "final",
        },
      },
    },
    confirming: {
      on: {
        CONFIRM: "paymentInfo",
        CHANGE_TRANSACTION: "calculator",
        CHANGE_RECEIVER: "receiversPhone",
      },
    },
    paymentInfo: {
      initial: "accepting",
      onDone: {
        target: "paying",
      },
      states: {
        accepting: {
          on: {
            CHANGE_NUMBER: {
              target: "validatingPhone",
              actions: assign({
                sendersMobile: (_, event) => event.value,
              }),
            },
            CHANGE_NET: {
              actions: assign({
                sendersNetwork: (_, event) => event.value,
              }),
            },
            SUBMIT: "submitting",
          },
        },
        validatingPhone: {
          invoke: {
            id: "verify-phone",
            src: (context) =>
              verifyPhone(
                context.sendersMobile,
                getCountry(context.countrySendingTo).alpha
              ),
            onError: {
              target: "invalid",
              actions: assign({
                errors: (context, event) => {
                  let { errors } = context;
                  errors.sendersMobile = event.data;
                  return errors;
                },
              }),
            },
            onDone: {
              target: "accepting",
            },
          },
        },
        submitting: {
          invoke: {
            src: validatePaymentInfo,
            id: "validate-payment-info",
            onDone: {
              target: "valid",
            },
            onError: {
              target: "invalid",
              actions: assign({
                errors: (context, event) => {
                  let { errors } = context;
                  Object.assign(errors, event.data);
                  return errors;
                },
              }),
            },
          },
        },
        invalid: {
          on: {
            CHANGE_NUMBER: {
              target: "validatingPhone",
              actions: assign({
                sendersMobile: (_, event) => event.value,
              }),
            },
            CHANGE_NET: {
              actions: assign({
                sendersNetwork: (_, event) => event.value,
              }),
              target: "submitting",
            },
          },
        },
        valid: {
          type: "final",
        },
      },
    },
    paying: {
      initial: "instructions",
      states: {
        instructions: {
          after: { 10000: { target: "received" } },
        },
        received: {
          after: { 10000: { target: "sent" } },
        },
        sent: {
          type: "final",
        },
      },
    },
  },
});

async function validateCalc(context) {
  let errors = {};
  let { countrySendingTo, amountToSend } = context;
  if (!countrySendingTo) {
    errors.countrySendingTo = "Country required";
  }
  if (!amountToSend) {
    errors.amountToSend = "Enter amount to send";
  }
  if (Object.keys(errors).length === 0) return true;
  else return Promise.reject(errors);
}

async function validateReceiversInfo(context) {
  let errors = {};
  let { receiversMobile, receiversNetwork } = context;
  if (!receiversMobile) {
    errors.receiversMobile = "Mobile is required";
  }
  if (!receiversNetwork) {
    errors.receiversNetwork = "Network is required";
  }
  if (Object.keys(errors).length === 0) return true;
  else return Promise.reject(errors);
}

async function validatePaymentInfo(context) {
  let errors = {};
  let { sendersMobile, sendersNetwork } = context;
  if (!sendersMobile) {
    errors.sendersMobile = "Mobile is required";
  }
  if (!sendersNetwork) {
    errors.sendersNetwork = "Network is required";
  }
  if (Object.keys(errors).length === 0) return true;
  else return Promise.reject(errors);
}

async function verifyPhone(number, code = "KE") {
  let pn = PhoneNumber(number, code);
  if (!pn.isValid()) {
    return Promise.reject("Phone number is invalid");
  }
  if (!pn.isMobile()) {
    return Promise.reject("Entered number is not mobile");
  }
  return true;
}

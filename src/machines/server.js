import { createMachine, interpret } from "xstate";

const transactionMachine = createMachine(
  {
    id: "single-transaction",
    initial: "validating",
    states: {
      validating: {
        initial: "csrfToken",
        onDone: "initializingTransaction",
        states: {
          csrfToken: {
            invoke: {
              src: "verifyCsrfToken",
              onDone: { target: "inputs" },
              onError: { target: "invalid" },
            },
          },
          inputs: {
            invoke: {
              src: "validateInputs",
              onDone: { target: "phone" },
              onError: { target: "invalid" },
            },
          },
          phone: {
            invoke: {
              src: "validatePhone",
              onDone: { target: "forexAndCharges" },
              onError: { target: "invalid" },
            },
          },
          forexAndCharges: {
            invoke: {
              src: "reComputeForexAndCharges",
              onDone: { target: "valid" },
              onError: { target: "invalid" },
            },
          },
          invalid: {},
          valid: { type: "final" },
        },
      },
      initializingTransaction: {
        invoke: {
          src: "saveToDB",
          onDone: "receivingPayment",
        },
      },
      receivingPayment: {
        onDone: {
          target: "sendingToReceiver",
          actions: [
            "doubleEntry",
            "updateTransactionStatusOnDB",
            "notifySender",
          ],
        },
        initial: "idle",
        states: {
          idle: {
            after: {
              5000: {
                target: "checking",
              },
            },
          },
          checking: {
            invoke: {
              src: "checkTransactionStatusFromAPI",
              onDone: {
                target: "received",
                actions: "updateTransactionStatusOnDB",
              },
              onError: { target: "idle" },
            },
          },
          received: { type: "final" },
        },
      },
      sendingToReceiver: {
        onDone: {
          target: "success",
        },
        initial: "sending",
        states: {
          sending: {
            invoke: {
              src: "sendToReceiver",
              onDone: {
                target: "sent",
                actions: [
                  "updateTransactionStatus",
                  "doubleEntry",
                  "notifySender",
                  "notifyReceiver",
                ],
              },
              onError: {
                target: "fail",
              },
            },
          },
          fail: {
            actions: [
              "updateTransactionStatus",
              "doubleEntry",
              "alertEmployee",
            ],
          },
          sent: { type: "final" },
        },
      },
      success: {
        type: "final",
      },
    },
  },
  {
    services: {
      verifyCsrfToken: async function (c, e) {
        return true;
      },
      validateInputs: async function (c, e) {
        return Promise.reject(false);
      },
      validatePhone: async function (c, e) {
        return true;
      },
      reComputeForexAndCharges: async function (c, e) {
        return true;
      },
    },
  }
);

const server = interpret(transactionMachine, { devTools: true }).start();

export default server;

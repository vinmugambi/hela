const countries = [
  {
    name: "Kenya",
    alpha: "KE",
    callingCode: "+254",
    min: 200,
    charges: 40,
    currency: {
      code: "KES",
      name: "Kenya shillings",
    },
    wallets: ["saf", "ake", "tke"],
  },
  {
    name: "Uganda",
    alpha: "UG",
    min: 6000,
    charges: 1500,
    currency: {
      code: "UGX",
      name: "Uganda shillings",
    },
    callingCode: "+256",
    wallets: ["aug", "mtn"],
  },
  {
    name: "Tanzania",
    alpha: "TZ",
    charges: 800,
    min: 4000,
    currency: {
      code: "TZS",
      name: "Tanzania shillings",
    },
    callingCode: "+255",
    wallets: ["vod", "atz"],
  },
];

const wallets = [
  {
    name: "Safaricom MPESA",
    provider: "Safaricom",
    paybill: 970970,
    code: "saf",
    instructions: [
      "Open MPESA on your phone",
      "Select LIPA na MPESA",
      "Select Paybill",
    ],
  },
  {
    name: "Airtel KE Money",
    provider: "Airtel Kenya",
    paybill: 420420,
    code: "ake",
    instructions: ["Open Airtel Money on your phone", "Select Pay bill"],
  },
  {
    name: "Airtel UG Money",
    provider: "Airtel Uganda ",
    paybill: 510510,
    code: "aug",
    instructions: ["Open Airtel Money on your phone", "Select Pay bill"],
  },
  {
    name: "Telkom Kash",
    code: "tke",
    paybill: 210210,
    provider: "Telkom Kenya",
    instructions: ["Open Telkom Kash on your phone", "Select Pay bill"],
  },
  {
    name: "MTN Momo",
    code: "mtn",
    paybill: 910910,
    provider: "MTN Uganda",
    instructions: ["Open MTN momo on your phone", "Select Pay bill"],
  },
  {
    name: "Vodacom MPESA",
    code: "vod",
    paybill: 610610,
    provider: "Vodacom Tanzania",
    instructions: ["Open MPESA on your phone", "Select Pay bill"],
  },
  {
    name: "Airtel TZ Money",
    code: "atz",
    paybill: 780780,
    provider: "Airtel Tanzania",
    instructions: ["Open Airtel Money on your phone", "Select Pay bill"],
  },
];

export function getCountryNames() {
  return countries.map((country) => country.name);
}

export function getCountry(name) {
  if (!name || typeof name !== "string") return;
  return countries.find(
    (country) => country.name.toLowerCase() === name.toLowerCase()
  );
}

export function getCurrencyCode(countryName) {
  let country = getCountry(countryName);
  if (country) {
    return country.currency.code;
  } else return "";
}

export function getNetworksByCountry(countryName) {
  let country = getCountry(countryName);
  if (!country) return [];
  return wallets.filter((wallet) => country.wallets.includes(wallet.code));
}

export function getNetwork(name) {
  return wallets.find((network) => network.name === name);
}

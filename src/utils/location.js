export function getUserCoordinates() {
  return new Promise(function (resolve, reject) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        function (position) {
          let lat = position.coords.latitude;
          let lon = position.coords.longitude;
          resolve({ lon, lat });
        },
        function () {
          reject(
            "Unable to retrieve location. Check whether your device has location enabled and connected to the internet"
          );
        }
      );
    } else reject("Your browser doesn't support geolocation");
  });
}

export async function extractCountryName(coordinates) {
  let { lat, lon } = coordinates;
  let url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lon}`;
  try {
    let res = await fetch(url);
    let data = await res.json();
    if (res.ok) {
      if (data.address) return Promise.resolve(data.address.country);
      else return Promise.reject(data.error);
    } else return Promise.reject(data.message);
  } catch (err) {
    return Promise.reject(err.message);
  }
}

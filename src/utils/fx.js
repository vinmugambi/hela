const fx = {
  _currency: {
    from: null,
    to: null,
  },
  _rates: {
    UGX: { buy: 0.0312, sell: 0.0315 },
    TZS: { buy: 0.043, sell: 0.046 },
    USD: { buy: 106.2, sell: 110.2 },
    KES: { buy: 1, sell: 1 },
  },
  _supported: function (code) {
    if (Object.keys(this._rates).includes(code)) {
      return true;
    } else throw new Error("Currency not supported");
  },
  _toKsh: function (amount) {
    return this._rates[this._currency.from].buy * amount;
  },
  _fromKsh: function (amount) {
    return (1 / this._rates[this._currency.to].sell) * amount;
  },
  init: function (from, to) {
    this._currency.from = from;
    this._currency.to = to;
  },
  from: function (from) {
    if (this._supported(from)) this._currency.from = from;
  },
  to: function (to) {
    if (this._supported(to)) this._currency.to = to;
  },

  exchange: function (amount) {
    if (Number.isNaN(amount)) throw new Error("Amount must be a number");
    if (!this._currency.from) throw new Error("Source currency is not defined");
    if (!this._currency.to)
      throw new Error("Destination currency is not defined");
    amount = Number(amount);
    let value;
    if (this._currency.to === this._currency.from) value = amount;
    else if (this._currency.to === "KES") value = this._toKsh(amount);
    else if (this._currency.from === "KES") value = this._fromKsh(amount);
    else {
      let inKsh = this._toKsh(amount);
      value = this._fromKsh(inKsh);
    }
    let rate = value / amount || 1; // if amount is 0 divide by 1;
    return {value, rate}
  },
};

export default function exchange(amount, from, to) {
  if (!amount) amount = 0;
  amount = Number(amount);
  if (!from) from = "KES";
  if (!to) to = "KES";
  fx.init(from, to);
  return fx.exchange(amount);
}
